<?php
// Include the composer autoloading file
require __DIR__ . '/vendor/autoload.php';

// Default timezone
date_default_timezone_set('Europe/Amsterdam');

// Load the .env file
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// Load the configuration file
\BHLBot\Utilities\Config::init();

// Load the store
\BHLBot\Utilities\Store::init();

// Set the proper locale
\Carbon\Carbon::setLocale('nl');

setlocale(LC_TIME, 'nl');

// Initialize and start the client
$client = new \BHLBot\Client\Client();

// Run in infinite loop to reconnect after failures
while(true) {
    $client->start();
}