<?php

namespace BHLBot\Commands;

use BHLBot\Api\GarlicClient;
use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;
use BHLBot\Utilities\Store;

/**
 * Class Garlic
 * @package BHLBot\Commands
 */
class Garlic implements Command
{

    /**
     * @param Client $client
     * @param Message $message
     * @return mixed
     */
    public function handle(Client $client, Message $message)
    {
        $target = $message->getTarget()->isMe() ? $message->getSender() : $message->getTarget();

        if (count($message->getCommandParameters()) === 0) {
            $address = $this->getDefaultAddress($message);

            if (!$address) {
                $client->send(new ChannelMessage($target->getName(), 'Geen adres opgegeven'));
                return;
            }
        } else {
            $address = trim($message->getCommandParameters()[0]);

            if ($message->getCommandParameters()[0] === 'save') {

                if (count($message->getCommandParameters()) < 2) {
                    $client->send(new ChannelMessage($target->getName(), 'Geen adres opgegeven'));
                    return;
                }

                $address = trim($message->getCommandParameters()[1]);

                Store::set($message->getSender() . '.garlic_address', $address);
            }
        }

        $balance = (new GarlicClient())->call($address);

        if ($balance === null || (is_array($balance) && array_key_exists('error', $balance))) {
            $client->send(new ChannelMessage($target->getName(), 'Adres niet gevonden'));
            return;
        }

        $client->send(new ChannelMessage($target->getName(), sprintf('%s GRLC', $balance)));
    }

    /**
     * @param Message $message
     * @return bool|mixed
     */
    private function getDefaultAddress(Message $message)
    {
        if (Store::get($message->getSender() . '.garlic_address')) {
            return Store::get($message->getSender() . '.garlic_address');
        }

        return false;
    }

}