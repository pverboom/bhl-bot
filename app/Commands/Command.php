<?php

namespace BHLBot\Commands;

use BHLBot\Client\Client;
use BHLBot\Utilities\Message;

/**
 * Interface Command
 * @package BHLBot\Commands
 */
interface Command
{

    /**
     * @param Client $client
     * @param Message $message
     * @return mixed
     */
    public function handle(Client $client, Message $message);

}