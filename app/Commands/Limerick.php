<?php

namespace BHLBot\Commands;

use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class Limerick
 * @package BHLBot\Commands
 */
class Limerick implements Command {

    /**
     * @param Client $client
     * @param Message $message
     */
    public function handle(Client $client, Message $message)
    {
        $limerick = $this->getRandomLimerick();

        $target = $message->getTarget()->isMe() ? $message->getSender() : $message->getTarget();

        foreach($limerick as $row) {
            $client->send(new ChannelMessage($target->getName(), $row));
            usleep(500000);
        }
    }

    /**
     * @return mixed
     */
    private function getRandomLimerick()
    {
        $limericks = include(base_path('data/limerick.php'));
        $key = array_rand($limericks);
        return $limericks[$key];
    }
}