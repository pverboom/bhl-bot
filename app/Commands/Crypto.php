<?php

namespace BHLBot\Commands;

use BHLBot\Api\CryptoClient;
use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class Crypto
 * @package BHLBot\Commands
 */
class Crypto implements Command
{
    /**
     * @param Client $client
     * @param Message $message
     *
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $target = $message->getTarget()->isMe() ? $message->getSender() : $message->getTarget();

        if (count($message->getCommandParameters()) === 0) {
            $client->send(new ChannelMessage($target->getName(), 'Geen coin opgegeven'));
            return;
        }

        $cryptoClient = new CryptoClient();

        $coin = trim(strtolower($message->getCommandParameters()[0]));

        $response = $cryptoClient->call($coin, ['convert' => 'EUR']);

        if (!is_array($response) || isset($response['error'])) {
            $client->send(new ChannelMessage($target->getName(), 'Coin niet gevonden'));
            return;
        }

        $data = reset($response);

        $client->send(
            new ChannelMessage(
                $target->getName(),
                sprintf(
                    '%s/EUR: %s (1h: %s, 24h: %s, 7d: %s)',
                    $data['symbol'] === 'CND' ? 'POEP' : $data['symbol'],
                    round($data['price_eur'], 5),
                    $this->plus($data['percent_change_1h']),
                    $this->plus($data['percent_change_24h']),
                    $this->plus($data['percent_change_7d'])
                )
            )
        );

        if ($coin === 'ripple' && (float)$data['price_eur'] < 0.5) {
            $client->send(new ChannelMessage($target->getName(),'Kut ripple!'));
        }
    }

    /**
     * @param $value
     * @return string
     */
    private function plus($value)
    {
        if ($value > 0) {
            return chr(3) . '3+' . $value . '%' . chr(3);
        }
        if ($value < 0) {
            return chr(3) . '4' . $value . '%' . chr(3);
        }

        return chr(3) . '2' . $value . '%' . chr(3);
    }

}