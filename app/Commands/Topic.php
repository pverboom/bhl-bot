<?php

namespace BHLBot\Commands;

use BHLBot\Client\Client;
use BHLBot\Responses\SetTopic;
use BHLBot\Utilities\Message;

/**
 * Class Topic
 * @package BHLBot\Commands
 */
class Topic implements Command
{
    /**
     * @param Client $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $topic = \BHLBot\Utilities\Topic::get();

        $client->send(new SetTopic($message->getTarget()->getName(), $topic));
    }

}