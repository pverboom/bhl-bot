<?php

namespace BHLBot\Commands;

use BHLBot\Api\ApiClient;
use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class NewQuote
 * @package BHLBot\Commands
 */
class NewQuote implements Command
{

    /**
     * @param Client $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $apiClient = new ApiClient();

        $parameter = implode(' ', $message->getCommandParameters());

        if (is_null($parameter)) {
            $quote = $apiClient->call('quotes/nieuw');
        } else {
            $quote = $apiClient->call('quotes/nieuw', ['user' => $parameter]);
        }

        if (!is_null(array_get($quote, 'error'))) {
            $client->send(new ChannelMessage(
                    $message->getTarget()->getName(), 'Geen quote gevonden')
            );

            return;
        }

        $contents = sanitize_quote($quote['data']['quote']);

        $client->send(new ChannelMessage(
            $message->getTarget()->getName(), sprintf('#%s - %s: %s', $quote['data']['id'], $quote['data']['user'], $contents))
        );

        if (strlen($contents) > 434) {
            $client->send(
                new ChannelMessage(
                    $message->getTarget()->getName(), sprintf('Bekijk de complete quote: %s', $quote['data']['url'])
                )
            );
        }
    }

}