<?php

namespace BHLBot\Commands;

use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class Say
 * @package BHLBot\Commands
 */
class Say implements Command {

    /**
     * @param Client $client
     * @param Message $message
     *
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $parameters = $message->getContentParameters();
        $channel = array_shift($parameters);
        
        if ($message->getTarget()->isMe() &&
            $message->getSender()->getName() == 'Peut' &&
            strpos($channel, '#') === 0
        ) {
            $client->send(new ChannelMessage($channel, implode(' ', $parameters)));
        }
    }
}