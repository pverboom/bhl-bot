<?php

namespace BHLBot\Commands;

use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;
use BHLBot\Utilities\ResponseQueue;

class Whois implements Command {

    public function handle(Client $client, Message $message)
    {
        $nickname = array_get($message->getCommandParameters(), 0);

        $channel = $message->getTarget()->getName();

        ResponseQueue::push('whois', function(Client $client, Message $message) use ($channel)
        {
            $parameters = explode(" ", $message->getContent());
            $response = sprintf('%s@%s', array_get($parameters, 1), array_get($parameters, 2));
            $client->send(new ChannelMessage($channel, $response));
        });

        $client->send(new \BHLBot\Responses\Whois($nickname));
    }

}