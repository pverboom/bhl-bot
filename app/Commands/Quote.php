<?php

namespace BHLBot\Commands;

use BHLBot\Api\ApiClient;
use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class Quote
 * @package BHLBot\Commands
 */
class Quote implements Command
{

    /**
     * @param Client $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $apiClient = new ApiClient();

        $parameter = trim(implode(' ', $message->getCommandParameters()));

        if (is_null($parameter)) {
            $quote = $apiClient->call('quotes/random');
        } elseif (preg_match('/^[0-9]+$/', $parameter)) {
            $quote = $apiClient->call(sprintf('quotes/%s', $parameter));
        } else {
            $quote = $apiClient->call('quotes/random', ['user' => $parameter]);
        }

        if (is_null($quote) || !is_null(array_get($quote, 'error'))) {
            $client->send(new ChannelMessage(
                $message->getTarget()->getName(), 'Geen quote gevonden')
            );
            return;
        }

        $contents = sanitize_quote($quote['data']['quote']);

        $client->send(new ChannelMessage(
                $message->getTarget()->getName(), sprintf('#%s - %s: %s', $quote['data']['id'], $quote['data']['user'], $contents)
            )
        );

        if (strlen($contents) > 434) {
            $client->send(
                new ChannelMessage(
                    $message->getTarget()->getName(), sprintf('Bekijk de complete quote: %s', $quote['data']['url'])
                )
            );
        }
    }

}