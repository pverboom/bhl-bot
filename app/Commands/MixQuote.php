<?php

namespace BHLBot\Commands;

use BHLBot\Api\ApiClient;
use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;
use const JSON_PRETTY_PRINT;

/**
 * Class MixQuote
 * @package BHLBot\Commands
 */
class MixQuote implements Command
{
    /**
     * @param Client  $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $theme = trim(implode(' ', $message->getCommandParameters()));

        $apiClient = new ApiClient();

        $quote = $apiClient->call('quotes/mixquote/' . strtolower($theme));

        if (!array_get($quote, 'data.quote')) {
            debug("Failed to retieve mixquote: \n\n" . json_encode($quote, JSON_PRETTY_PRINT) . "\n\n");
        }

        $client->send(new ChannelMessage($message->getTarget()->getName(),
            array_get($quote, 'data.quote', 'Mixquote niet gevonden')));
    }
}