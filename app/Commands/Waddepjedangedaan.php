<?php

namespace BHLBot\Commands;

use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class Waddepjedangedaan
 * @package BHLBot\Commands
 */
class Waddepjedangedaan implements Command
{
    /**
     * @param Client $client
     * @param Message $message
     *
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $name = $this->getRandomName();

        $target = $message->getTarget()->isMe() ? $message->getSender() : $message->getTarget();

        $client->send(new ChannelMessage($target->getName(), $name));
    }

    /**
     * @return string
     */
    private function getRandomName()
    {
        $names = $this->names();

        return sprintf('%s %s%s',
            $this->getRandomPart($names, 0),
            $this->getRandomPart($names, 1),
            $this->getRandomPart($names, 2)
        );
    }

    /**
     * @param $names
     * @param $part
     * @return mixed
     */
    private function getRandomPart($names, $part)
    {
        return trim($names[mt_rand(0, count($names) - 1)][$part]);
    }

    /**
     * @return string
     */
    private function names()
    {
        $names = include(base_path('data/names.php'));

        return array_map(function($name) {
            return explode(" ", trim($name));
        }, explode("\n", $names));
    }

}