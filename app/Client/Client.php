<?php

namespace BHLBot\Client;

/**
 * Class Client
 * @package BHLBot\Client
 */
use BHLBot\Responses\InitializeUser;
use BHLBot\Responses\JoinChannel;
use BHLBot\Responses\Pong;
use BHLBot\Responses\PrivateMessage;
use BHLBot\Responses\Response;
use BHLBot\Responses\SetNickname;
use BHLBot\Utilities\Message;

/**
 * Class Client
 * @package BHLBot\Client
 */
class Client
{
    /**
     * @var
     */
    private $socket;

    /**
     * @var
     */
    private $listeners;

    /**
     * @var
     */
    private $timers;

    /**
     * @var bool
     */
    private $joined = false;

    /**
     * @var int
     */
    private $connected_at;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->registerListeners();
        $this->registerTimers();
    }

    /**
     *
     */
    public function start()
    {
        $this->socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        socket_connect($this->socket, config('server.host'), config('server.port'));

        $this->send(new InitializeUser(
            config('client.ident'),
            config('server.host'),
            config('client.hostname'),
            config('client.realname')
        ));

        $this->send(new SetNickname(config('client.nickname')));

        $this->connected_at = time();
        debug($this->connected_at);

        $read   = [$this->socket];
        $write  = [];
        $except = [];

        while (true) {
            if (socket_select($read, $write, $except, 0, 10) > 0) {
                $data = @socket_read($this->socket, 65536, PHP_NORMAL_READ);

                if ($data === false) {
                    debug('Failed to read from socket');
                    break;
                }

                $this->update($data);
            }

            $this->checkTimers();
            usleep(10000);
            $read = [$this->socket];
        }

        $this->joined = false;

        sleep(5);
    }

    /**
     * @param Response $response
     */
    public function send(Response $response)
    {
        $command = $response->getCommand();

        debug(sprintf("SENDING: %s", $command));

        socket_write($this->socket, $command . "\r\n");
    }

    /**
     * @param $data
     */
    private function update($data)
    {
        if (!$this->joined && time() > ($this->connected_at + 2)) {
            $this->send(new PrivateMessage('NickServ', sprintf('INFO %s', config('client.nickname'))));
            $this->join();
        }

        if ($data == "\n") return;

        $this->dispatch(new Message($data));
    }

    /**
     * @param Message $message
     */
    private function dispatch(Message $message)
    {
        if (config('debug') == 2) {
            debug(sprintf("getType: [%s]",                $message->getType()));
            debug(sprintf("getSender: [%s]",              $message->getSender()));
            debug(sprintf("getTarget: [%s]",              $message->getTarget()));
            debug(sprintf("getContent: [%s]",             $message->getContent()));
            debug(sprintf("getContentParameters: [%s]",   implode(',', $message->getContentParameters())));
            debug(sprintf("getCommand: [%s]",             $message->getCommand()));
            debug(sprintf("getCommandParameters: [%s]", implode(',', $message->getCommandParameters())));
            debug('------------');
        }

        if (!$message->getType()) {
            return;
        }

        if (array_key_exists($message->getType(), $this->listeners)) {
            $this->listeners[$message->getType()]->handle($this, $message);
            return;
        }

        // Reply to PONG as sender
        if ((string)$message->getSender() === 'PING') {
            debug('Got PING, sending PONG');
            $this->send(new Pong($message->getType()));
            return;
        }

        debug(sprintf("No listener for %s", $message->getType()));
    }

    /**
     * Joins the channels from the configuration file
     */
    private function join()
    {
        foreach(config('server.channels') as $channel) {
            $this->send(new JoinChannel($channel));
        }

        $this->joined = true;
    }

    /**
     * Register the listeners for specific commands from the config file
     */
    private function registerListeners()
    {
        foreach(config('listeners') as $command => $listener) {
            $this->listeners[$command] = new $listener;
        }
    }

    /**
     * Register timed commands
     */
    private function registerTimers()
    {
        foreach(config('timers') as $name => $timer) {
            $this->timers[$name] = new $timer($this);
        }
    }

    /**
     * Checks the registered timers
     */
    private function checkTimers()
    {
        foreach($this->timers as $timer) {
            $timer->check();
        }
    }
}