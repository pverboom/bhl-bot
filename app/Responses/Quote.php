<?php

namespace BHLBot\Responses;

/**
 * Class Quote
 * @package BHLBot\Responses
 */
class Quote implements Response
{

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var array
     */
    protected $quote;

    /**
     * Quote constructor.
     * @param $channel
     * @param $quote
     */
    public function __construct($channel, array $quote)
    {
        $this->channel = $channel;
        $this->quote = $quote;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        $message = sprintf(
            '#%s - %s: %s',
            $this->quote['data']['id'],
            $this->quote['data']['user'],
            sanitize_quote($this->quote['data']['quote'])
        );

        $response = new ChannelMessage($this->channel, $message);

        return $response->getCommand();
    }
}