<?php

namespace BHLBot\Responses;

/**
 * Class Pong
 * @package BHLBot\Responses
 */
class Pong implements Response
{
    /**
     * @var null
     */
    private $pong;

    /**
     * Pong constructor.
     * @param null $pong
     */
    public function __construct($pong = null)
    {
        $this->pong = $pong;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("PONG %s", $this->pong ?: (':' . config('server.host')));
    }

}