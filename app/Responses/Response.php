<?php

namespace BHLBot\Responses;

interface Response
{

    public function getCommand();

}