<?php

namespace BHLBot\Responses;

/**
 * Class InitializeUser
 * @package BHLBot\Responses
 */
/**
 * Class InitializeUser
 * @package BHLBot\Responses
 */
class InitializeUser implements Response
{
    /**
     * @var The ident string of the client
     */
    private $ident;

    /**
     * @var The hostname of the client
     */
    private $hostname;

    /**
     * @var The server hostname
     */
    private $host;

    /**
     * @var The real name of the client
     */
    private $realname;

    /**
     * InitializeUser constructor.
     * @param $ident
     * @param $hostname
     * @param $host
     * @param $realname
     */
    public function __construct($ident, $hostname, $host, $realname)
    {
        $this->ident    = $ident;
        $this->hostname = $hostname;
        $this->host     = $host;
        $this->realname = $realname;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("USER %s %s %s:%s", $this->ident, $this->hostname, $this->host, $this->realname);
    }

}