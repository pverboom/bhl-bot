<?php

namespace BHLBot\Responses;

/**
 * Class ChannelMessage
 * @package BHLBot\Responses
 */
class ChannelMessage implements Response
{

    /**
     * @var
     */
    protected $channel;

    /**
     * @var
     */
    protected $message;

    /**
     * ChannelMessageResponse constructor.
     * @param $channel
     * @param $message
     */
    public function __construct($channel, $message)
    {
        $this->channel = $channel;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCommand()
    {
        return sprintf("PRIVMSG %s %s", $this->channel, $this->message);
    }
}