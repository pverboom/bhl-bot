<?php

namespace BHLBot\Responses;

/**
 * Class Whois
 * @package BHLBot\Responses
 */
class Whois implements Response
{

    /**
     * @var
     */
    private $nickname;

    /**
     * Whois constructor.
     * @param $nickname
     */
    public function __construct($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf('WHOIS %s', $this->nickname);
    }

}