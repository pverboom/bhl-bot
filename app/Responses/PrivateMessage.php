<?php

namespace BHLBot\Responses;

/**
 * Class PrivateMessage
 * @package BHLBot\Responses
 */
class PrivateMessage implements Response
{

    /**
     * @var
     */
    protected $nickname;

    /**
     * @var
     */
    protected $message;

    /**
     * ChannelMessageResponse constructor.
     * @param $nickname
     * @param $message
     */
    public function __construct($nickname, $message)
    {
        $this->nickname = $nickname;
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("PRIVMSG %s %s", $this->nickname, $this->message);
    }

}