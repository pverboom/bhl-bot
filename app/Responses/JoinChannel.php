<?php

namespace BHLBot\Responses;

/**
 * Class JoinChannel
 * @package BHLBot\Responses
 */
class JoinChannel implements Response
{

    /**
     * @var
     */
    protected $channel;

    /**
     * JoinChannel constructor.
     * @param $channel
     */
    public function __construct($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("JOIN %s", $this->channel);
    }

}