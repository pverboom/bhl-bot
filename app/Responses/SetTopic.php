<?php

namespace BHLBot\Responses;

/**
 * Class SetTopic
 * @package BHLBot\Responses
 */
/**
 * Class SetTopic
 * @package BHLBot\Responses
 */
class SetTopic implements Response
{

    /**
     * @var
     */
    private $topic;

    /**
     * @var
     */
    private $channel;

    /**
     * SetNickname constructor.
     * @param $channel
     * @param $topic
     */
    public function __construct($channel, $topic)
    {
        $this->channel = $channel;
        $this->topic = $topic;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("TOPIC %s %s", $this->channel, $this->topic);
    }

}