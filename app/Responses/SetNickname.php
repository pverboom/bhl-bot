<?php

namespace BHLBot\Responses;

/**
 * Class SetNickname
 * @package BHLBot\Responses
 */
class SetNickname implements Response
{

    /**
     * @var
     */
    private $nickname;

    /**
     * SetNickname constructor.
     * @param $nickname
     */
    public function __construct($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("NICK %s", config('client.nickname'));
    }

}