<?php

namespace BHLBot\Responses;

/**
 * Class ChanServ
 * @package BHLBot\Responses
 */
class ChanServ implements Response
{

    /**
     * @var
     */
    protected $command;

    /**
     * @var
     */
    protected $parameters = [];

    /**
     * ChannelMessageResponse constructor.
     * @param $nickname
     * @param $message
     */
    public function __construct($command)
    {
        $this->command = $command;

        $arguments = func_get_args();
        $this->parameters = array_slice($arguments, 1);
    }

    /**
     * @return mixed
     */
    public function getCommand()
    {
        return sprintf("PRIVMSG CHANSERV %s %s", $this->command, implode(' ', $this->parameters));
    }

}