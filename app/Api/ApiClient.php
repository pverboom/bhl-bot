<?php

namespace BHLBot\Api;

/**
 * Class ApiClient
 * @package BHLBot\Api
 */
class ApiClient {

    /**
     * @param $endpoint
     * @param array $parameters
     * @return mixed
     */
    public function call($endpoint, $parameters = [])
    {
        $url = sprintf('%s/%s?%s', config('api.url'), $endpoint, http_build_query($parameters));

        $request = curl_init($url);

        curl_setopt($request, CURLOPT_HTTPHEADER, [
            sprintf('Api-Key: %s', config('api.key')),
            'Content-Type: application/json'
        ]);

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($request);

        curl_close($request);

        return json_decode($response, true);
    }

}