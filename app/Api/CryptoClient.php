<?php

namespace BHLBot\Api;

/**
 * Class CryptoClient
 * @package BHLBot\Api
 */
class CryptoClient {

    /**
     * @param $coin
     * @param array $parameters
     * @return mixed
     */
    public function call($coin, $parameters = [])
    {
        $url = sprintf('https://api.coinmarketcap.com/v1/ticker/%s/?%s', $coin, http_build_query($parameters));

        $request = curl_init($url);

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($request);

        curl_close($request);

        return json_decode($response, true);
    }

}