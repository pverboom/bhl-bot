<?php

namespace BHLBot\Api;

/**
 * Class GarlicClient
 * @package BHLBot\Api
 */
class GarlicClient {

    /**
     * @param $address
     * @return mixed
     */
    public function call($address)
    {
        $url = sprintf('https://explorer.grlc-bakery.fun/ext/getbalance/%s', $address);

        $request = curl_init($url);

        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($request, CURLOPT_FOLLOWLOCATION, true);

        $response = curl_exec($request);

        curl_close($request);

        return json_decode($response, true);
    }

}