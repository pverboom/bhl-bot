<?php

namespace BHLBot\Utilities;

/**
 * Class Message
 * @package BHLBot\Utilities
 */
/**
 * Class Message
 * @package BHLBot\Utilities
 */
/**
 * Class Message
 * @package BHLBot\Utilities
 */
/**
 * Class Message
 * @package BHLBot\Utilities
 */
class Message
{

    /**
     * @var
     */
    private $parts;

    /**
     * Message constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->parts = explode(" ", $data);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        $type = irc_trim(array_get($this->parts, 1));

        if ($type == config('server.host')) {
            return irc_trim(array_get($this->parts, 0));
        }

        return $type;
    }
    /**
     * @return Target
     */
    public function getSender()
    {
        $type = irc_trim(array_get($this->parts, 1));

        if ($type == config('server.host')) {
            return irc_trim(array_get($this->parts, 1));
        }

        return new Target(array_get($this->parts, 0));
    }

    /**
     * @return Target
     */
    public function getTarget()
    {
        return new Target(array_get($this->parts, 2));
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return irc_trim(implode(" ", array_slice($this->parts, 3)));
    }

    /**
     * @return array
     */
    public function getContentParameters()
    {
        return array_slice($this->parts, 3);
    }

    /**
     * @return bool
     */
    public function getCommand()
    {
        $command = irc_trim(array_get($this->parts, 3));
        
        if ($this->getType() == 'PRIVMSG' && strpos($command, '!') === 0) {
            return substr($command, 1);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getCommandParameters()
    {
        if ($this->getCommand() !== false) {
            return array_map('trim', array_slice($this->parts, 4));
        }

        return [];
    }
}