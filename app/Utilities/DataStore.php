<?php

namespace BHLBot\Utilities;

/**
 * Class DataStore
 * @package BHLBot\Utilities
 */
class DataStore
{

    /**
     * @var array
     */
    private static $data = [];

    /**
     * @param $name
     * @param $value
     */
    public static function put($name, $value)
    {
        static::$data[strtolower($name)] = $value;
    }

    /**
     * @param $name
     * @param $default
     * @return mixed|null
     */
    public static function get($name, $default = null)
    {
        return array_get(static::$data, strtolower($name), $default);
    }

}