<?php

namespace BHLBot\Utilities;

/**
 * Class Config
 * @package BHLBot\Utilities
 */
class Config
{
    /**
     * @var
     */
    private static $config;

    /**
     * Initialize the configuration values
     */
    public static function init()
    {
        static::$config = require base_path('config.php');
    }

    /**
     * @param $key
     * @param null $default
     * @return null
     */
    public static function get($key, $default = null)
    {
        $keys = explode('.', $key);

        $setting = static::$config;
        
        foreach($keys as $key) {
            if (!is_array($setting) || !array_key_exists($key, $setting)) {
                return $default;
            }

            $setting = $setting[$key];
        }

        return $setting;
    }


}