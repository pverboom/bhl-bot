<?php

namespace BHLBot\Utilities;

/**
 * Class Target
 * @package BHLBot\Utilities
 */
/**
 * Class Target
 * @package BHLBot\Utilities
 */
class Target {

    /**
     * @var
     */
    private $target;

    /**
     * Target constructor.
     * @param $target
     */
    public function __construct($target)
    {
        $this->target = irc_trim($target);
    }

    /**
     * @return bool
     */
    public function isClient()
    {
        return !$this->isServer() && !$this->isChannel();
    }

    /**
     * @return bool
     */
    public function isServer()
    {
        return $this->target == config('server.host');
    }

    /**
     * @return bool
     */
    public function isChannel()
    {
        return strpos($this->target, '#') === 0;
    }

    /**
     * @return bool
     */
    public function isMe()
    {
        return $this->isClient() && $this->getName() == config('client.nickname');
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        if ($this->isClient()) {
            return current(explode('!', $this->target));
        }

        return $this->target;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        if ($this->isClient()) {
            return implode('!', array_slice(explode('!', $this->target), 1));
        }

        return $this->target;
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this->target;
    }
}