<?php

namespace BHLBot\Utilities;

/**
 * Class ResponseQueue
 * @package BHLBot\Utilities
 */
class ResponseQueue
{

    /**
     * @var array
     */
    protected static $queue = [];

    /**
     * @var array
     */
    private static $allowed = ['whois'];

    /**
     * @param $type
     * @param $callback
     */
    public static function push($type, $callback)
    {
        if (!array_key_exists($type, static::$queue)) {
            static::$queue[$type] = [];
        }

        array_push(static::$queue[$type], $callback);
    }

    /**
     * @param $type
     * @return callable|null
     */
    public static function get($type)
    {
        return array_shift(static::$queue[$type]);
    }

}