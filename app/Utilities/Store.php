<?php

namespace BHLBot\Utilities;

/**
 * Class Store
 * @package BHLBot\Utilities
 */
class Store {

    /**
     * @var
     */
    private static $data;

    /**
     * Initialize the configuration storage file
     */
    public static function init()
    {
        $file = base_path('config.json');

        if (!file_exists($file)) {
            file_put_contents($file, '{}');
        }

        static::$data = json_decode(file_get_contents($file), true);
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        return array_get(static::$data, $key, $default);
    }

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value)
    {
        static::$data[$key] = $value;
        static::save();
    }

    /**
     * Persist the data to the filesystem
     */
    public static function save()
    {
        file_put_contents('config.json', json_encode(self::$data));
    }

}