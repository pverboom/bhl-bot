<?php

namespace BHLBot\Utilities;

use BHLBot\Api\ApiClient;
use Carbon\Carbon;

/**
 * Class Topic
 * @package BHLBot\Utilities
 */
class Topic
{
    /**
     * @var
     */
    private static $color;

    /**
     * @return string
     */
    public static function get()
    {
        self::$color = '3';

        $apiClient = new ApiClient();
        $result = $apiClient->call('activiteiten');

        $activities = collect(array_get($result, 'data', []))
            ->sortBy(function($activity) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $activity['starts_at'])->timestamp;
            })
            ->reduce(function($topic, $activity) {
                $string = ($topic == '' ? '' : ' | ') . self::getActivityString($activity);
                return $topic . (strlen($topic . $string) <= 307 ? $string : '');
            }, '');

        return substr('Welkom!' . (!empty($activities) ? ' | Agenda: ' . $activities : ''), 0, 290);
    }

    /**
     * @param array $activity
     * @return string
     */
    public static function getActivityString(array $activity)
    {
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $activity['starts_at']);

        return sprintf('%s%s (%s): %s%s',
            self::nextColor(),
            $activity['name'],
            $activity['signed_up'],
            $date->format('j M \'y'),
            self::getColor('')
        );
    }

    /**
     * @return string
     */
    private static function nextColor()
    {
        self::$color = self::$color == '' ? '3' : '';
        return self::getColor(self::$color);
    }

    /**
     * @param $color
     * @return string
     */
    private static function getColor($color)
    {
        return sprintf("%s%s", chr(3), $color);
    }
}
