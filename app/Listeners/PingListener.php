<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Responses\Pong;
use BHLBot\Utilities\Message;

/**
 * Class PingListener
 * @package BHLBot\Listeners
 */
class PingListener implements Listener {

    /**
     * @param Client $client
     * @param Message $message
     */
    public function handle(Client $client, Message $message)
    {
        debug($message->getContent());
        $client->send(new Pong());
    }

}