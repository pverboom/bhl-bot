<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Responses\ChanServ;
use BHLBot\Responses\NickServ;
use BHLBot\Responses\PrivateMessage;
use BHLBot\Utilities\Message;

/**
 * Class NoticeListener
 * @package BHLBot\Listeners
 */
class NoticeListener implements Listener {

    /**
     * @param Client $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        // Check for NickServ replies
        if ($message->getSender()->getName() == 'NickServ') {
            $this->handleNickServ($client, $message);
        }
    }

    /**
     * @param Client $client
     * @param Message $message
     */
    protected function handleNickServ(Client $client, Message $message)
    {
        $nickname = config('client.nickname');
        $password = config('client.nickserv.password');

        debug(sprintf("NickServ: %s",$message->getContent()));

        if (strpos($message->getContent(), 'isn\'t registered.') !== false) {
            $client->send(new NickServ('REGISTER', $password));
        } elseif ($message->getContent() == sprintf('%s is Bot', $nickname)) {
            $client->send(new NickServ('IDENTIFY', $password));
            $client->send(new ChanServ('OP'));
        }
    }
}