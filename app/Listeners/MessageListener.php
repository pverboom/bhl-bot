<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;
use function starts_with;
use function strtolower;

/**
 * Class MessageListener
 * @package BHLBot\Listeners
 */
class MessageListener implements Listener {

    /**
     * @param Client $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $command = $message->getCommand();

        // Handle !commands
        if ($command && array_key_exists($command, config('commands', []))) {
            $class = config(sprintf('commands.%s', $message->getCommand()));

            if ($class) {
                $command = new $class;
                $command->handle($client, $message);

                return;
            }
        }

        // Reply when a user greets the bot
        if($message->getContent() == sprintf("%s!", config('client.nickname')) && $message->getTarget()->isChannel()) {

            $name = $message->getSender()->getName();

            if (str_contains(strtolower($name), 'jeffrey') || str_contains(strtolower($name), 'kattenvrouwtje')) {
                $client->send(new ChannelMessage(
                    $message->getTarget()->getName(), sprintf("%s! Met je dikke kop!", $name)
                ));
                return;
            }

            $client->send(new ChannelMessage($message->getTarget()->getName(), sprintf("%s!", $name)));

            return;
        }

        // Jeffrey heeft een dikke kop
        if(str_contains(strtolower($message->getContent()), 'dikke kop') && $message->getTarget()->isChannel()) {
            $client->send(new ChannelMessage(
                $message->getTarget()->getName(), "Jeffrey heeft een dikke kop!"
            ));
        }

        if (strtolower($message->getContent()) === 'hoe bhlbot?') {
            $client->send(new ChannelMessage(
                $message->getTarget()->getName(),
                sprintf("Kanker %s!", $message->getSender()->getName())
            ));
            return;
        }

        // Hoe <x>? Kanker<x>!
        $matches = [];
        if (preg_match('/hoe[\s]{0,1}([^\s]+)\?$/i', $message->getContent(), $matches) && $message->getTarget()->isChannel()) {
            $client->send(new ChannelMessage(
                $message->getTarget()->getName(),
                sprintf("Kanker%s!", $matches[1])
            ));
        }
    }


}