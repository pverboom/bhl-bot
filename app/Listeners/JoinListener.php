<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\Message;

/**
 * Class JoinListener
 * @package BHLBot\Listeners
 */
class JoinListener implements Listener
{

    /**
     * @param Client $client
     * @param Message $message
     * @return void
     */
    public function handle(Client $client, Message $message)
    {
        $channel = $message->getTarget()->getName();

        if (!$message->getSender()->isMe()) {
            if ($message->getSender()->getName() == 'Jeffrey') {
                $client->send(new ChannelMessage($channel, "Hé Jeffrey, met je dikke kop!"));
            }
        }
    }

}