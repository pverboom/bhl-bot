<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Utilities\Message;
use BHLBot\Utilities\ResponseQueue;

/**
 * Class WhoisListener
 * @package BHLBot\Listeners
 */
class WhoisListener implements Listener
{
    /**
     * @param Client  $client
     * @param Message $message
     * @return mixed|void
     */
    public function handle(Client $client, Message $message)
    {
        $callback = ResponseQueue::get('whois');

        if ($callback) {
            call_user_func($callback, $client, $message);
        }
    }
}