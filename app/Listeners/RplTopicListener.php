<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Utilities\DataStore;
use BHLBot\Utilities\Message;

/**
 * Class RplTopicListener
 * @package BHLBot\Listeners
 */
class RplTopicListener implements Listener
{
    /**
     * @param Client $client
     * @param Message $message
     * @return void;
     */
    public function handle(Client $client, Message $message)
    {
        $key = sprintf('%s.topic', $message->getTarget()->getName());

        $parameters = $message->getContentParameters();

        $channel = array_shift($parameters);
        $topic = irc_trim(implode(' ', $parameters));

        if (is_null(DataStore::get($key))) {
            DataStore::put(sprintf('%s.topic', $channel), $topic);
        }
    }
}