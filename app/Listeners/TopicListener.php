<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Utilities\DataStore;
use BHLBot\Utilities\Message;

/**
 * Class TopicListener
 * @package BHLBot\Listeners
 */
class TopicListener implements Listener
{
    /**
     * @param Client $client
     * @param Message $message
     * @return void;
     */
    public function handle(Client $client, Message $message)
    {
        DataStore::put(sprintf('%s.topic', $message->getTarget()->getName()), $message->getContent());
    }
}