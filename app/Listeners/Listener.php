<?php

namespace BHLBot\Listeners;

use BHLBot\Client\Client;
use BHLBot\Utilities\Message;

/**
 * Interface Listener
 * @package BHLBot\Listeners
 */
interface Listener {

    /**
     * @param Client $client
     * @param Message $message
     * @return mixed
     */
    public function handle(Client $client, Message $message);

}