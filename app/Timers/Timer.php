<?php

namespace BHLBot\Timers;

use BHLBot\Client\Client;

/**
 * Class Timer
 * @package BHLBot\Timers
 */
abstract class Timer
{

    /**
     * @var
     */
    protected $time;

    /**
     * @var
     */
    protected $client;

    /**
     * Timer constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;

        $this->time = time() + 10;
    }

    /**
     * @return mixed
     */
    abstract public function getInterval();

    /**
     * @return mixed
     */
    abstract public function handle();

    /**
     * Checks if the interval is reached
     */
    public function check()
    {
        if ($this->time > time()) {
            return;
        }

        $this->time = time() + $this->getInterval();
        $this->handle();
    }

}