<?php

namespace BHLBot\Timers;

use BHLBot\Api\ApiClient;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\DataStore;

/**
 * Class Quote
 * @package BHLBot\Timers
 */
final class Quote extends Timer
{

    /**
     * @return int
     */
    public function getInterval()
    {
        return 300;
    }

    /**
     *
     */
    public function handle()
    {
        $apiClient = new ApiClient();
        $quote = $apiClient->call('quotes/nieuw');

        if (is_null($quote)) {
            return;
        }

        $previousQuote = DataStore::get('quote.new');

        if (is_null($previousQuote) || (int)array_get($quote, 'data.id') <= (int)$previousQuote) {
            DataStore::put('quote.new', array_get($quote, 'data.id'));
            return;
        }

        DataStore::put('quote.new', array_get($quote, 'data.id'));

        foreach (config('server.channels') as $channel) {
            $this->sendNewQuote($channel, $quote);
        }
    }

    /**
     * @param $channel
     * @param $quote
     */
    private function sendNewQuote($channel, $quote)
    {
        $contents = sanitize_quote(array_get($quote, 'data.quote'));

        $this->client->send(
            new ChannelMessage($channel, sprintf(
                    'Nieuwe quote! #%s - %s: %s',
                    array_get($quote, 'data.id'),
                    array_get($quote, 'data.user'),
                    $contents
                )
            )
        );

        if (strlen($contents) > 434) {
            $this->client->send(new ChannelMessage($channel, sprintf('Bekijk de complete quote: %s', $quote['data']['url'])));
        }
    }

}