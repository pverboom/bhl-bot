<?php

namespace BHLBot\Timers;

use BHLBot\Responses\SetTopic;
use BHLBot\Utilities\DataStore;

/**
 * Class Topic
 * @package BHLBot\Timers
 */
final class Topic extends Timer
{

    /**
     * @return int
     */
    public function getInterval()
    {
        return 900;
    }

    /**
     *
     */
    public function handle()
    {
        $topic = \BHLBot\Utilities\Topic::get();

        foreach(config('server.channels') as $channel) {
            $channelTopic = DataStore::get(sprintf('%s.topic', $channel));

            if (is_null($channelTopic)) {
                debug('No topic for ' . $channel);
                continue;
            }

            if ($channelTopic != $topic) {
                $this->client->send(new SetTopic($channel, $topic));
            }
        }
    }

}