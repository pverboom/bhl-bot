<?php

namespace BHLBot\Timers;

use BHLBot\Api\ApiClient;
use BHLBot\Responses\ChannelMessage;
use BHLBot\Utilities\DataStore;

/**
 * Class Comment
 * @package BHLBot\Timers
 */
final class Comment extends Timer
{

    /**
     * @return int
     */
    public function getInterval()
    {
        return 60;
    }

    /**
     *
     */
    public function handle()
    {
        $apiClient = new ApiClient();
        $comments = $apiClient->call('reacties');

        if (is_null($comments) || !array_get($comments, 'data.0')) {
            return;
        }

        $comment = array_get($comments, 'data.0');

        $previousComment = DataStore::get('comment.new');

        if (is_null($previousComment) || (int)$comment['id'] <= (int)$previousComment) {
            DataStore::put('comment.new', $comment['id']);
            return;
        }

        DataStore::put('comment.new', $comment['id']);

        foreach (config('server.channels') as $channel) {
            $this->sendNewComment($channel, $comment);
        }
    }

    /**
     * @param $channel
     * @param $comment
     */
    private function sendNewComment($channel, $comment)
    {
        $this->client->send(
            new ChannelMessage($channel, sprintf(
                    'Nieuwe reactie door %s op "%s": %s',
                    $comment['user'],
                    $comment['title'],
                    $comment['comment']
                )
            )
        );

        $this->client->send(new ChannelMessage($channel, sprintf('Bekijk de reactie: %s', $comment['url'])));
    }

}