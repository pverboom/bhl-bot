<?php

return [

    'debug' => getenv('DEBUG'), // 0 = no messages, 1 = debug messages, 2 = full message logging

    'server' => [

        'host'      => 'irc.blokhutlan.nl',

        'port'      => 6667,

        'channels'  => explode(',', getenv('CHANNELS'))

    ],

    'client' => [

        'nickname'  => getenv('NICKNAME'),

        'realname'  => 'BHL Bot',

        'ident'     => 'BlokhutLAN',

        'hostname'  => '192.168.100.241',

        'nickserv'  => [

            'password'  => 'kipsnoekel1912'

        ]
    ],

    'commands' => [

        'limerick'          => \BHLBot\Commands\Limerick::class,

        'whois'             => \BHLBot\Commands\Whois::class,

        'quote'             => \BHLBot\Commands\Quote::class,

        'mixquote'          => \BHLBot\Commands\MixQuote::class,

        'nieuw'             => \BHLBot\Commands\NewQuote::class,

        'zoek'              => \BHLBot\Commands\SearchQuote::class,

        'topic'             => \BHLBot\Commands\Topic::class,

        'say'               => \BHLBot\Commands\Say::class,

        'waddepjedangedaan' => \BHLBot\Commands\Waddepjedangedaan::class,

        'crypto'            => \BHLBot\Commands\Crypto::class,

        'garlic'            => \BHLBot\Commands\Garlic::class,

    ],

    'timers' => [

        'topic'     => \BHLBot\Timers\Topic::class,

        'quote'     => \BHLBot\Timers\Quote::class,

        'comment'   => \BHLBot\Timers\Comment::class,

    ],

    'api' => [

        'url'   => getenv('BHL_API_URL'),

        'key'   => getenv('BHL_API_KEY'),

    ],

    'listeners' => [

        'PING'      => \BHLBot\Listeners\PingListener::class,

        'JOIN'      => \BHLBot\Listeners\JoinListener::class,

        'PRIVMSG'   => \BHLBot\Listeners\MessageListener::class,

        'NOTICE'    => \BHLBot\Listeners\NoticeListener::class,

        'TOPIC'     => \BHLBot\Listeners\TopicListener::class,

        '332'       => \BHLBot\Listeners\RplTopicListener::class,

        '311'       => \BHLBot\Listeners\WhoisListener::class,

        // Ignore this stuff
        '001'       => \BHLBot\Listeners\VoidListener::class,
        '002'       => \BHLBot\Listeners\VoidListener::class,
        '003'       => \BHLBot\Listeners\VoidListener::class,
        '004'       => \BHLBot\Listeners\VoidListener::class,
        '005'       => \BHLBot\Listeners\VoidListener::class,
        '251'       => \BHLBot\Listeners\VoidListener::class,
        '252'       => \BHLBot\Listeners\VoidListener::class,
        '254'       => \BHLBot\Listeners\VoidListener::class,
        '255'       => \BHLBot\Listeners\VoidListener::class,
        '265'       => \BHLBot\Listeners\VoidListener::class,
        '266'       => \BHLBot\Listeners\VoidListener::class,
        '353'       => \BHLBot\Listeners\VoidListener::class,
        '366'       => \BHLBot\Listeners\VoidListener::class,
        '372'       => \BHLBot\Listeners\VoidListener::class,
        '375'       => \BHLBot\Listeners\VoidListener::class,
        '376'       => \BHLBot\Listeners\VoidListener::class,

    ],

];
