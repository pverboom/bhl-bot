<?php
/**
 * @return string
 */
function base_path($path = '')
{
    return __DIR__ . (!empty($path) ? DIRECTORY_SEPARATOR . $path : '');
}

/**
 * @param $key
 * @param string $default
 * @return mixed
 */
function config($key, $default = '')
{
    return \BHLBot\Utilities\Config::get($key, $default);
}

/**
 * @param $message
 */
function debug($message)
{
    if (config('debug') > 0) {
        echo sprintf("[%s] %s\n", date('H:i:s'), $message);
    }
}

/**
 * @param $string
 * @return string
 */
function irc_trim($string)
{
    $string = strpos($string, ':') === 0 ? substr($string, 1) : $string;

    return trim($string);
}

/**
 * @param $string
 * @return mixed
 */
function irc_replace($string)
{
    $characters = [chr(2)];

    foreach($characters as $character) {
        $string = str_replace($character, '', $string);
    }

    return $string;
}

/**
 * @param $quote
 * @return mixed
 */
function sanitize_quote($quote)
{
    $quote = trim($quote, "\r\n");
    $quote = str_replace("\r", "", $quote);
    $quote = str_replace("\n", " -> ", $quote);

    while (strpos($quote, " ->  -> ") !== false) {
        $quote = str_replace(" ->  -> ", " -> ", $quote);
    }

    return $quote;
}

